package dam.androidjavi.proyectoarduino.sqlite;

public class HistoryDataAbstract {
    public static final String TABLE_NAME ="history";

    public static final String ID = "id";
    public static final String DEVICE = "device";
    public static final String USER = "user";
    public static final String CREATED_AT = "created_at";
    public static final String ACTION = "action";
}
