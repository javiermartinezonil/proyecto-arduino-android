package dam.androidjavi.proyectoarduino.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import dam.androidjavi.proyectoarduino.models.HistoryData;

public class HistoryDataHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Lawyers.db";
    private static final String LOG_TAG = "SQLITE_HISTORY";

    private SQLiteDatabase db;

    public HistoryDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + HistoryDataAbstract.TABLE_NAME + " ("
                + HistoryDataAbstract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + HistoryDataAbstract.USER + " TEXT NOT NULL,"
                + HistoryDataAbstract.DEVICE + " TEXT NOT NULL,"
                + HistoryDataAbstract.CREATED_AT + " TEXT NOT NULL,"
                + HistoryDataAbstract.ACTION + " TEXT NOT NULL,"
                + "UNIQUE (" + HistoryDataAbstract.ID + "))");

        this.db = sqLiteDatabase;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void saveHistory(ArrayList<HistoryData> arrayList) {
        for (HistoryData hd : arrayList) {

            if (this.getHistoryDataById(hd.getId()) == null) {//CHECK EXIST IN DATABASE
                this.insertHistoryData(hd); //INSERT HISTORY DATA
            }else {
                this.updateHistoryData(hd);//UPDATE HISTORY DATA
            }

        }

    }

    public int updateHistoryData(HistoryData hd){
        return getWritableDatabase().update(
                HistoryDataAbstract.TABLE_NAME,
                hd.toContentValues(),
                HistoryDataAbstract.ID + " LIKE ?",
                new String[]{hd.getId() + ""}
        );
    }

    public void insertHistoryData(HistoryData hd){
        getWritableDatabase().insert(
                HistoryDataAbstract.TABLE_NAME,
                null,
                hd.toContentValues()
        );
    }

    public HistoryData getHistoryDataById(int id) {
        String query = "select * from " + HistoryDataAbstract.TABLE_NAME + " WHERE id=" + id;
        Cursor c = getWritableDatabase().rawQuery(query, null);

        if (c.moveToFirst()) {
            String device = c.getString(c.getColumnIndex(HistoryDataAbstract.DEVICE));
            String user = c.getString(c.getColumnIndex(HistoryDataAbstract.USER));
            String created = c.getString(c.getColumnIndex(HistoryDataAbstract.CREATED_AT));
            String action = c.getString(c.getColumnIndex(HistoryDataAbstract.ACTION));

            return new HistoryData(
                    id,
                    user,
                    device,
                    created,
                    action
            );

        }

        return null;
    }

    public ArrayList<HistoryData> getHistory() {
        ArrayList<HistoryData> arrayList = new ArrayList<>();

        Cursor c = getReadableDatabase().query(
                HistoryDataAbstract.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        while (c.moveToNext()) {
            int id = c.getInt(c.getColumnIndex(HistoryDataAbstract.ID));
            String device = c.getString(c.getColumnIndex(HistoryDataAbstract.DEVICE));
            String user = c.getString(c.getColumnIndex(HistoryDataAbstract.USER));
            String created = c.getString(c.getColumnIndex(HistoryDataAbstract.CREATED_AT));
            String action = c.getString(c.getColumnIndex(HistoryDataAbstract.ACTION));

            arrayList.add(new HistoryData(
                    id,
                    user,
                    device,
                    created,
                    action
            ));
        }
        return arrayList;
    }
}
