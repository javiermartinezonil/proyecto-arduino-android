package dam.androidjavi.proyectoarduino.recyclerview;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidjavi.proyectoarduino.R;
import dam.androidjavi.proyectoarduino.models.HistoryData;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<HistoryData> historyDataArrayList;
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder{
        ConstraintLayout constraintLayout;

        TextView tvDate;
        TextView tvName;
        ImageView imagePuerta;

        public MyViewHolder(@NonNull ConstraintLayout constraintLayout) {
            super(constraintLayout);
            this.constraintLayout = constraintLayout;

            this.tvDate = constraintLayout.findViewById(R.id.tvDate);
            this.tvName = constraintLayout.findViewById(R.id.tvName);
            this.imagePuerta = constraintLayout.findViewById(R.id.imagePuerta);
        }

        public void bind(final HistoryData hd, final OnItemClickListener listener){
            this.tvDate.setText(hd.getStringTime());
            this.tvName.setText(hd.getUser());

            if (hd.getAction().equals("open")){
                this.imagePuerta.setImageResource(R.drawable.ic_bloquear);
            }else {
                this.imagePuerta.setImageResource(R.drawable.ic_desbloquear);
            }

            //LISTENER
            this.constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("CLICK", "NORMAL CLICK");
                    listener.onItemClick(hd);
                }
            });

        }
    }

    public interface OnItemClickListener{
        void onItemClick(HistoryData hd);
    }

    public MyAdapter(ArrayList<HistoryData> historyDataArrayList, OnItemClickListener listener){
        this.historyDataArrayList = historyDataArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout constraintLayout = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        return new MyViewHolder(constraintLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(historyDataArrayList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return historyDataArrayList.size();
    }

}
