package dam.androidjavi.proyectoarduino.ui.dashboard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import dam.androidjavi.proyectoarduino.R;
import dam.androidjavi.proyectoarduino.ws.AsyncTaskGetHistory;
import dam.androidjavi.proyectoarduino.ws.AsyncTaskSendSignal;

import static android.view.View.GONE;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private Button btn1, btn2, btn3, btn4;
    private ArrayList<Button> buttonArrayList;
    private View view;
    private int contDevices;
    private String name;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        this.view = root;
        setup(root);

        return root;
    }

    private void setup(View view) {
        this.buttonArrayList = new ArrayList<>();

        this.btn1 = view.findViewById(R.id.btn1);
        this.btn2 = view.findViewById(R.id.btn2);
        this.btn3 = view.findViewById(R.id.btn3);
        this.btn4 = view.findViewById(R.id.btn4);

        this.buttonArrayList.add(btn1);
        this.buttonArrayList.add(btn2);
        this.buttonArrayList.add(btn3);
        this.buttonArrayList.add(btn4);

        this.name = getValuePreference(this.view.getContext(), "user");

        for (int i = 1; i <= 4; i++) {
            final int finalI = i;
            this.buttonArrayList.get(i - 1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AsyncTaskSendSignal(finalI, name).execute();
                }
            });
        }

        setupButtons();

    }

    private void setupButtons() {
        this.contDevices = 0;
        for (int i = 1; i <= 4; i++) {
            String value = getValuePreference(this.view.getContext(), "dispositivo" + i);
            if (!value.equals("")){
                this.contDevices++;
                this.buttonArrayList.get(i - 1).setText(value.toUpperCase());
            }else {
                this.buttonArrayList.get(i - 1).setVisibility(GONE);
            }
        }
    }

    public String getValuePreference(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }
}