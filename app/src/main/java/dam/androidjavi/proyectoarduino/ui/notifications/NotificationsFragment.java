package dam.androidjavi.proyectoarduino.ui.notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import dam.androidjavi.proyectoarduino.R;
import dam.androidjavi.proyectoarduino.ws.AsyncTaskSendDevices;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.LinearLayout.*;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private View view;

    //
    private Button btnSave, btnAdd, btnRm;
    private EditText et1, et2, et3, et4;
    private TextView tvDispositivos;
    private LinearLayout ly1, ly2, ly3, ly4;
    private int contDevices;

    private ArrayList<EditText> editTextArrayList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        this.view = root;

        setup();

        return root;
    }

    private void setup() {

        this.editTextArrayList = new ArrayList<>();
        this.et1 = this.view.findViewById(R.id.etDispositivo1);
        this.et2 = this.view.findViewById(R.id.etDispositivo2);
        this.et3 = this.view.findViewById(R.id.etDispositivo3);
        this.et4 = this.view.findViewById(R.id.etDispositivo4);
        this.tvDispositivos = this.view.findViewById(R.id.tvDispositivos);

        this.editTextArrayList.add(et1);
        this.editTextArrayList.add(et2);
        this.editTextArrayList.add(et3);
        this.editTextArrayList.add(et4);

        this.ly1 = this.view.findViewById(R.id.ly1);
        this.ly2 = this.view.findViewById(R.id.ly2);
        this.ly3 = this.view.findViewById(R.id.ly3);
        this.ly4 = this.view.findViewById(R.id.ly4);

        this.btnSave = this.view.findViewById(R.id.btSave);
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

        this.btnAdd = this.view.findViewById(R.id.btnAdd);
        this.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDevices();
            }
        });

        this.btnRm = this.view.findViewById(R.id.btnRm);
        this.btnRm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeDevices();
            }
        });

        setupDevices();

    }

    private void saveData(){
        String device1 = String.valueOf(et1.getText());
        String device2 = String.valueOf(et2.getText());
        String device3 = String.valueOf(et3.getText());
        String device4 = String.valueOf(et4.getText());

        saveValuePreference(view.getContext(), "dispositivo1", device1);
        saveValuePreference(view.getContext(), "dispositivo2", device2);
        saveValuePreference(view.getContext(), "dispositivo3", device3);
        saveValuePreference(view.getContext(), "dispositivo4", device4);

        new AsyncTaskSendDevices(device1, device2, device3, device4).execute();
        Toast.makeText(view.getContext(), "Dispositivos guardados.", Toast.LENGTH_LONG).show();

    }

    private void setupDevices() {
        this.contDevices = 0;
        for (int i = 1; i <= 4; i++) {
            String value = getValuePreference(view.getContext(), "dispositivo" + i);
            if (!value.equals("")) {
                this.contDevices++;
                this.editTextArrayList.get(i - 1).setText(value);
            }
        }

        this.tvDispositivos.setText("Dispositivos " + this.contDevices + "/4");
        this.showHideDevices(this.contDevices);
    }

    private void showHideDevices(int cont) {
        switch (cont) {
            case 1:
                this.ly1.setVisibility(View.VISIBLE);
                this.ly2.setVisibility(View.GONE);
                this.ly3.setVisibility(View.GONE);
                this.ly4.setVisibility(View.GONE);
                break;
            case 2:
                this.ly1.setVisibility(View.VISIBLE);
                this.ly2.setVisibility(View.VISIBLE);
                this.ly3.setVisibility(View.GONE);
                this.ly4.setVisibility(View.GONE);
                break;
            case 3:
                this.ly1.setVisibility(View.VISIBLE);
                this.ly2.setVisibility(View.VISIBLE);
                this.ly3.setVisibility(View.VISIBLE);
                this.ly4.setVisibility(View.GONE);
                break;
            case 4:
                this.ly1.setVisibility(View.VISIBLE);
                this.ly2.setVisibility(View.VISIBLE);
                this.ly3.setVisibility(View.VISIBLE);
                this.ly4.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void addDevices() {
        if (this.contDevices == 4){
            Toast.makeText(view.getContext(), "No se pueden añadir más dispositivos", Toast.LENGTH_LONG).show();
        }

        if (this.contDevices < 4) {
            this.contDevices++;
            this.btnAdd.setVisibility(View.VISIBLE);
            showHideDevices(this.contDevices);
        }

        this.tvDispositivos.setText("Dispositivos " + this.contDevices + "/4");
    }

    private void removeDevices() {
        if (this.contDevices == 1){
            Toast.makeText(view.getContext(), "No se pueden eliminar más dispositivos", Toast.LENGTH_LONG).show();
        }

        if (this.contDevices > 1) {
            this.contDevices--;
            this.btnAdd.setVisibility(View.VISIBLE);
            showHideDevices(this.contDevices);

            this.editTextArrayList.get(this.contDevices).setText("");
            saveData();
        }

        this.tvDispositivos.setText("Dispositivos " + this.contDevices + "/4");
    }

    public void saveValuePreference(Context context, String key, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();
        editor.putString(key, text);
        editor.commit();
    }

    public String getValuePreference(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }
}