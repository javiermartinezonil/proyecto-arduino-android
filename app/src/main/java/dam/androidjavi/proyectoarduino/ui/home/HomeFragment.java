package dam.androidjavi.proyectoarduino.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import dam.androidjavi.proyectoarduino.R;
import dam.androidjavi.proyectoarduino.models.HistoryData;
import dam.androidjavi.proyectoarduino.recyclerview.MyAdapter;
import dam.androidjavi.proyectoarduino.sqlite.HistoryDataHelper;
import dam.androidjavi.proyectoarduino.ws.AsyncTaskGetHistory;

public class HomeFragment extends Fragment implements MyAdapter.OnItemClickListener{

    private HomeViewModel homeViewModel;
    private View view;
    private ArrayList<HistoryData> historyDataArrayList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static final String url = "http://domotica.martinezmorcillo.com/api/";
    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        this.view = root;

        setup();

        return root;
    }

    private void setup() {

        //RECYCLER VIEW
        this.historyDataArrayList = new HistoryDataHelper(getContext()).getHistory(); //GET HISOTRY
        this.recyclerView = this.view.findViewById(R.id.recyclerViewHistory);
        this.recyclerView.setHasFixedSize(true);
        this.setHistory(this.historyDataArrayList);

        //AÑADIMOS LA LÍNEA SEPARADORA
        this.recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this.view.getContext(), RecyclerView.VERTICAL, false));

        //REFRESH HISTORY
        this.swipeRefreshLayout = (SwipeRefreshLayout) this.view.findViewById(R.id.swipeRefreshLayout);
        this.swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                swipeRefreshLayout.setRefreshing(true);
                try {
                    new AsyncTaskGetHistory(getContext()).execute().get();
                    setHistory(new HistoryDataHelper(getContext()).getHistory()); //GET HISTORY
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setHistory(ArrayList<HistoryData> hdAux) {
        this.recyclerView.setAdapter(new MyAdapter(hdAux, this));
    }

    @Override
    public void onItemClick(HistoryData hd) {
        Log.i("HISTORY", hd.getUser());
    }
}