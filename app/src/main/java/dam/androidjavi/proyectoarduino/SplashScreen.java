package dam.androidjavi.proyectoarduino;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import dam.androidjavi.proyectoarduino.ws.AsyncTaskGetHistory;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //OCULTAR BARRA
        getSupportActionBar().hide();


    }

    @Override
    protected void onResume() {
        super.onResume();
        configSplash();

    }

    private void configSplash() {

        Thread timer = new Thread() {
            @Override
            public void run() {
                super.run();
                try {

                    sleep(3000);

                    Intent intent;
                    if (hasArduinoId()) {
                        intent = new Intent(SplashScreen.this, Home.class);
                        saveValuePreference(getApplicationContext(), "dispositivo1", "Dispositivo 1");
                    } else {
                        intent = new Intent(SplashScreen.this, MainActivity.class);
                        new AsyncTaskGetHistory(getApplicationContext()).execute();
                    }

                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        timer.start();


    }

    private boolean hasArduinoId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("arduino", 0);
        SharedPreferences.Editor editor = pref.edit();

        return pref.getString("id", null) != null;
    }

    public void saveValuePreference(Context context, String key, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();
        editor.putString(key, text);
        editor.commit();
    }

}