package dam.androidjavi.proyectoarduino.ws;


import android.os.AsyncTask;
import android.util.Log;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class AsyncTaskSendSignal extends AsyncTask<String, String, String> {

    private static final String url = "http://domotica.martinezmorcillo.com/api/";
    private int signal;
    private String name;

    public AsyncTaskSendSignal(int signal, String name){
        this.signal = signal;
        this.name = name;
    }

    public void postData() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url + "changeData?");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("device", signal + ""));
            nameValuePairs.add(new BasicNameValuePair("pass", "12345678"));
            nameValuePairs.add(new BasicNameValuePair("user", this.name));
            nameValuePairs.add(new BasicNameValuePair("action", "open"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            Log.i("TAG " + signal, String.valueOf(entity.getContent()));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected String doInBackground(String... args) {
        postData();
        return null;
    }

    //https://app.martinezmorcillo.com/api/history?pass=12345678
}
