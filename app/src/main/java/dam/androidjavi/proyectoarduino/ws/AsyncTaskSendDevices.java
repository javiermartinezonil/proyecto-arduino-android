package dam.androidjavi.proyectoarduino.ws;


import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class AsyncTaskSendDevices extends AsyncTask<String, String, String> {

    private static final String url = "http://domotica.martinezmorcillo.com/api/";
    private String name1, name2, name3, name4;

    public AsyncTaskSendDevices(String name1, String name2, String name3, String name4)
    {
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.name4 = name4;
    }

    public void postData() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url + "changeDevices?");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("device1", this.name1));
            nameValuePairs.add(new BasicNameValuePair("device2", this.name2));
            nameValuePairs.add(new BasicNameValuePair("device3", this.name3));
            nameValuePairs.add(new BasicNameValuePair("device4", this.name4));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected String doInBackground(String... args) {
        postData();
        return null;
    }

}
