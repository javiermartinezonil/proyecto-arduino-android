package dam.androidjavi.proyectoarduino.ws;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dam.androidjavi.proyectoarduino.models.HistoryData;
import dam.androidjavi.proyectoarduino.sqlite.HistoryDataHelper;

public class AsyncTaskGetHistory extends AsyncTask<String, Integer, Boolean> {

    private static String LOG_TAG = "ASYN_TASK_GET_HISOTRY";

    private static final String url = "http://domotica.martinezmorcillo.com/api/";

    private Context context;

    public AsyncTaskGetHistory(Context context){
        this.context = context;
    }

    public void getHistory() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url + "history?");

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("pass", "12345678"));
            nameValuePairs.add(new BasicNameValuePair("user", "Javi"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            String res = EntityUtils.toString(entity);
            JSONArray history = new JSONObject(res).getJSONArray("history");

            JSONObject aux;
            ArrayList<HistoryData> arrayList = new ArrayList<>();
            for (int i = 0; i < history.length(); i++) {
                aux = history.getJSONObject(i);
                arrayList.add(new HistoryData(
                        aux.getInt("id"),
                        aux.getString("user"),
                        aux.getString("device_changed"),
                        aux.getString("created_at"),
                        aux.getString("action")
                ));

            }

            new HistoryDataHelper(this.context).saveHistory(arrayList);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Boolean doInBackground(String... args) {
        getHistory();
        return null;
    }
}