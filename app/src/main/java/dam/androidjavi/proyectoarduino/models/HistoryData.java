package dam.androidjavi.proyectoarduino.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dam.androidjavi.proyectoarduino.sqlite.HistoryDataAbstract;

public class HistoryData {

    private int id;
    private String user;
    private String device;
    private String createdAt;
    private String action;
    private static ArrayList<HistoryData> historyDataArrayList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public HistoryData(int id, String user, String device, String createdAt, String action) {
        this.id = id;
        this.user = user;
        this.device = device;
        this.createdAt = createdAt;
        this.action = action;
    }

    public HistoryData() {
        this.id = -1;
        this.user = "";
        this.device = "";
        this.createdAt = "";
        this.action = "";
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(HistoryDataAbstract.ID, this.id);
        values.put(HistoryDataAbstract.USER, this.user);
        values.put(HistoryDataAbstract.DEVICE, this.device);
        values.put(HistoryDataAbstract.CREATED_AT, this.createdAt);
        values.put(HistoryDataAbstract.ACTION, this.action);
        return values;
    }

    @NonNull
    @Override
    public String toString() {
        return this.user + " " + this.device + " " + createdAt;
    }

    public ArrayList<HistoryData> getHistoryDataArrayList() {
        return historyDataArrayList;
    }

    public void setHistoryDataArrayList(ArrayList<HistoryData> arrayList) {
        historyDataArrayList = arrayList;
    }

    public String getAction() {
        return action;
    }

    public String getStringTime() {
        String texto = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date strDate = sdf.parse(this.createdAt);

            long timeNow = System.currentTimeMillis();
            long timeAction = strDate.getTime();
            long time = timeNow - timeAction;
            Date date = new Date(time);

            int months = date.getMonth();
            int days = date.getDay();
            int hours = date.getHours();
            int minutes = date.getMinutes();

            if (months > 1) {
                texto = "Hace " + months + " meses";
            } else if (days > 1) {
                texto = "Hace " + minutes + " días";
            } else if (days == 1) {
                texto = "Hace un día";
            } else if (hours > 1) {
                texto = "Hace " + minutes + " horas";
            } else if (hours == 1) {
                texto = "Hace una hora";
            } else if (minutes > 1) {
                texto = "Hace " + minutes + " minutos";
            } else {
                texto = "Hace un momento";
            }

        } catch (ParseException e) {
            texto = "Error al convertir la fecha.";
            e.printStackTrace();
        }

        return texto;
    }
}
